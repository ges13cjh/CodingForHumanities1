# First homework

Hi, my name is Alexander, and I'm both a graduate student in [Musicology](https://en.wikipedia.org/wiki/Musicology) and an undergraduate student in [Digital Humanities](http://whatisdigitalhumanities.com/). 

## Expectations
During the *Coding for Humanities* seminar, I hope to learn:

* something about data formats
* basics in XML, Python, and R
* how to work collaboratively on a DH project
