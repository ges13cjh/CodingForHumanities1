# Expectations about IntroDH

---

## My Personal Expectations

* understand varying definitions of [Digital Humanities](http://whatisdigitalhumanities.com/)
* get knowledge about data analysis with R
* have a nice and interesting *Praktium*
